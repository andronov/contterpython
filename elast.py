from elasticsearch import NotFoundError
from elasticsearch_dsl import DocType, field as fl
from elasticsearch_dsl.connections import connections
from sqlalchemy.orm import sessionmaker, scoped_session

from models import User, engine, BaseSite, BaseLink
from elastic import User as UserElastic, Site as SiteElastic, Link as LinkElastic

connections.create_connection(hosts=['localhost'])


class Signal(object):

    def __init__(self, session=None):
        self.session = session

    def get_or_create_elastic(self):
        pass

    def mapping(self, id, model, obj, many=False):
        print('mapping', id, model, obj)

        if many:
            array = []
            results = self.session.query(model).filter(model.id.in_([i.id for i in id]))
            for rest in results:
                array.append(self.mapping(rest.id, model, obj))
            return array

        res = self.session.query(model).filter(model.id == id).first()
        kwargs = {}

        if not res:
            return kwargs

        for key, item in self.get_items(obj):
            print('KEY', key, item)
            if hasattr(res, key):
                if hasattr(item, 'model'):
                    print(getattr(res, key), item.model, getattr(item, 'many', False))
                    kwargs[key] = self.mapping(getattr(res, key), item.model, item.doc_type,
                                               getattr(item, 'many', False))
                else:
                    value = getattr(res, key)

                    kwargs[key] = value

        kwargs['meta'] = {'id': res.id}
        print(kwargs)


        try:
            r = obj.get(id=res.id)
            try:
                del(kwargs['meta'])
                r.update()
            except Exception as e:
                print(e)
            return r
        except NotFoundError:
            r = obj(**kwargs)
            r.save()
            return r

    def add_elastic(self, model, obj):
        kwargs = {}
        for key, item in self.get_items(obj):
            if hasattr(model, key) or key == 'site':
                if hasattr(model, 'site_id') and model.site_id and key == 'site':
                    kwargs['site'] = self.mapping(model.site_id, BaseSite, SiteElastic, False)
                elif key != 'site':
                    kwargs[key] = getattr(model, key)

        obj = obj({"id": model.id}, **kwargs)
        obj.save()

    @staticmethod
    def get_items(obj):
        """
        Получаем атрибуты DocType
        :param obj:
        :return:
        """
        return obj._doc_type.mapping.properties._params['properties'].items()


def main():
    session_factory = sessionmaker(bind=engine, expire_on_commit=False)
    session = scoped_session(session_factory)()
    signal = Signal(session=session)

    for item in session.query(User).all():
        signal.add_elastic(item, UserElastic)

    for item in session.query(BaseSite).all():
        signal.add_elastic(item, SiteElastic)

    #for item in session.query(BaseLink).all():
     #   signal.add_elastic(item, LinkElastic)


if __name__ == '__main__':
    main()