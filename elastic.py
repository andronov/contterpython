from elasticsearch_dsl import DocType, field as fl
from elasticsearch_dsl.connections import connections

from models import BaseSite

connections.create_connection(hosts=['localhost'])


class User(DocType):
    id = fl.Integer()
    first_name = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    last_name = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    username = fl.String(analyzer='snowball')
    email = fl.String()
    type = fl.Integer()
    display_name = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    color = fl.String()
    description = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    avatar = fl.String()
    city = fl.String()
    location = fl.String()
    website = fl.String()
    timezone = fl.String()
    country = fl.String()
    is_active = fl.Boolean()
    is_ban = fl.Boolean()
    official = fl.Boolean()
    new = fl.Boolean()
    followers = fl.Integer()

    created = fl.Date()
    updated = fl.Date()
    last_visit = fl.Date()

    class Meta:
        index = 'boston'

    def save(self, **kwargs):
        return super(User, self).save(**kwargs)


class Site(DocType):
    id = fl.Integer()
    name = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    description = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    url = fl.String(analyzer='snowball')
    short_url = fl.String(analyzer='snowball')
    color = fl.String()
    favicon = fl.String()
    image = fl.String()
    rss = fl.String()
    lang = fl.String()
    followers = fl.Integer()

    created = fl.Date()
    updated = fl.Date()
    is_active = fl.Boolean()

    class Meta:
        index = 'boston'

    def save(self, **kwargs):
        return super(Site, self).save(**kwargs)


class Link(DocType):
    id = fl.Integer()
    title = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    description = fl.String(analyzer='snowball', fields={'raw': fl.Keyword()})
    keywords = fl.String(analyzer='snowball')

    user = fl.Nested(doc_class=User)
    site = fl.Nested(doc_class=Site)

    info = fl.Object()

    type = fl.Integer()
    rating = fl.Integer()
    index = fl.Float()
    url = fl.String(analyzer='snowball')
    full_url = fl.String()
    thumbnail = fl.String()
    medium_img = fl.String()

    is_active = fl.Boolean()
    created = fl.Date()
    updated = fl.Date()

    class Meta:
        index = 'boston'

    def save(self, **kwargs):
        return super(Link, self).save(**kwargs)


User.init()
Site.init()
Link.init()
"""
# create the mappings in elasticsearch
Article.init()

# create and save and article
article = Article(meta={'id': 42}, title='Hello world!', tags=['test'])
article.body = ''' looong text '''
article.published_from = datetime.now()
article.save()
"""

print(connections.get_connection().cluster.health())
