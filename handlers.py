import json
from web import BaseHandler
from tornado import gen
from admin.flasky import authenticateded

"""
#
# COLUMN
#
"""


class UserCreateColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserCreateColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    #@authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    #@authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'link' not in post:
            self.write({'failure': u'Error no link'})

        link = post['link']

        if link != 'validate':
            self.write({'failure': u'Error no link'})
        else:
            if link in 'base':
                self.write({'success': u'post in column'})
            else:
                self.write({'success': u'create column'})

        """
        print(user)
        print(self.request.body.decode('utf-8'))

        to_json = {}

        self.write({'result': to_json})
        """


"""
#
# COLUMN
#
"""


class UserDeleteColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserDeleteColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'column' not in post:
            self.write({'failure': u'column no link'})

        column = post['wall']

        if column in 'base wall':
            self.write({'failure': u'base column'})
        else:
            self.write({'success': u'delete column'})

"""
#
# COLUMN
#
"""


class UserCreateColumnWordHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserCreateColumnWordHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'name' not in post:
            self.write({'failure': u'Error no link'})

        name = post['name']

        if name != 'validate':
            self.write({'failure': u'Error no link'})
        else:
            if name in 'base':
                self.write({'success': u'post in column'})
            else:
                self.write({'success': u'create column'})

"""
#
# COLUMN
#
"""


class UserFollowColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserFollowColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'column' not in post:
            self.write({'failure': u'wall no link'})

        column = post['column']

        if column in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            # create follow column
            self.write({'success': u'delete wall'})


"""
#
# COLUMN
#
"""


class UserUnFollowColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserUnFollowColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'column' not in post:
            self.write({'failure': u'wall no link'})

        column = post['column']

        if column in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            # create follow column
            self.write({'success': u'delete wall'})


"""
#
# COLUMN
#
"""


class UserChangeSizeColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeSizeColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'size' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        size = post['size']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        else:
            # change in base in settings
            self.write({'success': u'create column'})


"""
#
# COLUMN
#
"""


class UserChangeTypeColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(UserChangeTypeColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'type' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        type = post['type']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        else:
            # change in base in settings
            self.write({'success': u'change type column'})


"""
#
# COLUMN
#
"""


class UserAddMinusWordColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(UserAddMinusWordColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'word' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        word = post['word']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        else:
            if word != 'validate':
                self.write({'failure': u'word not validate'})
            elif word in 'base':
                self.write({'failure': u'word have been in base'})
            else:
                self.write({'success': u'word-minus add in column'})


"""
#
# COLUMN
#
"""


class UserAddPlusWordColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(UserAddPlusWordColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'word' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        word = post['word']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        else:
            if word != 'validate':
                self.write({'failure': u'word not validate'})
            elif word in 'base':
                self.write({'failure': u'word have been in base'})
            else:
                self.write({'success': u'word-plus add in column'})


"""
#
# COLUMN
#
"""


class UserAddGroupShowColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(UserAddGroupShowColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'group' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        group = post['group']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        elif group != 'not user group in base':
            self.write({'failure': u'not user group in base'})
        else:
            if group != 'validate':
                self.write({'failure': u'word not validate'})
            else:
                self.write({'success': u'add column in group'})


"""
#
# COLUMN
#
"""


class UserAddColumnInWordColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(UserAddColumnInWordColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'columnadd' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        columnadd = post['columnadd']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        elif columnadd != 'not user group in base':
            self.write({'failure': u'not user group in base'})
        else:
            if columnadd != 'validate':
                self.write({'failure': u'word not validate'})
            else:
                self.write({'success': u'add column in group'})


"""
#
# COLUMN
#
"""


class UserEditSortColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        super(UserEditSortColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'sort' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        sort = post['columnadd']

        # exists
        if column != 'not user column in base':
            self.write({'failure': u'not user column in base'})
        else:
            if sort != 'validate':
                self.write({'failure': u'word not validate'})
            else:
                self.write({'success': u'add column in group'})


"""
#
# WALL
#
"""


class UserCreateWallHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserCreateWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'name' not in post and 'icon' not in post:
            self.write({'failure': u'Error no link'})

        name = post['name']
        icon = post['icon']

        if name != 'validate':
            self.write({'failure': u'Error no name'})
        if name in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            self.write({'success': u'create wall'})


"""
#
# WALL
#
"""


class UserDeleteWallHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserDeleteWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'wall' not in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']

        if wall in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            self.write({'success': u'delete wall'})


"""
#
# WALL
#
"""


class UserFollowWallHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserFollowWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'wall' not in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']

        if wall in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            # Create wall
            self.write({'success': u'delete wall'})


"""
#
# WALL
#
"""


class UserUnFollowWallHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserUnFollowWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'wall' not in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']

        if wall in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            # Create wall
            self.write({'success': u'delete wall'})


"""
#
# ITEM
#
"""


class UserAddFavouriteItemHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserAddFavouriteItemHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'item' not in post:
            self.write({'failure': u'wall no link'})

        item = post['item']

        if item in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            self.write({'success': u'delete wall'})


"""
#
# ITEM
#
"""


class UserDeleteFavouriteItemHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserDeleteFavouriteItemHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'item' not in post:
            self.write({'failure': u'wall no link'})

        item = post['item']

        if item in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            self.write({'success': u'delete wall'})


"""
#
# WALL IN DISPLAY | COLUMN IN DISPLAY | JSON
#
"""


class UserWallToJsonHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallToJsonHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'wall' not in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']

        if wall not in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            self.write({'success': u'json wall'})


"""
#
# COLUMN IN DISPLAY SCROLL | JSON
#
"""


class UserColumnScrollToJsonHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserColumnScrollToJsonHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'wall' not in post and not 'column' in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']
        column = post['column']
        page = post['page']

        if wall not in 'base wall':
            self.write({'failure': u'base wall'})
        if column not in 'base wall':
            self.write({'failure': u'base wall'})
        else:
            # page number more 10 items
            self.write({'success': u'json wall'})


"""
#
# USER SETTINGS | PROFILE
#
"""


class UserEditSettingsProfileHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserEditSettingsProfileHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'wall' not in post and not 'column' in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']

        if wall not in 'base wall':
            pass
        else:
            # page number more 10 items
            self.write({'success': u'json wall'})

"""
#
# SHOW COLLECTIONS
#
"""


class UserShowCollectionAddColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserShowCollectionAddColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'column' in post:
            self.write({'failure': u'wall no link'})

        wall = post['wall']

        if wall not in 'base wall':
            pass
        else:
            self.write({'success': u'json wall'})

"""
#
# COLUMN
#
"""


class UserCreateColumnRecontentHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserCreateColumnRecontentHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'name' not in post:
            self.write({'failure': u'Error no link'})

        name = post['name']

        if name != 'validate':
            self.write({'failure': u'Error no link'})
        else:
            if name in 'base':
                self.write({'success': u'post in column'})
            else:
                self.write({'success': u'create column'})
