import json

import datetime
import feedparser
import requests
from slugify import slugify
from wand.image import Image

from bs4 import BeautifulSoup
from sqlalchemy import text
from sqlalchemy.orm import sessionmaker, scoped_session

from models import engine, User, BaseLink, BaseSite, BaseLinkInfo
#from sravni import calculation_to_word

session_factory = sessionmaker(bind=engine, expire_on_commit=False)
Session = scoped_session(session_factory)

import threading


class TimeoutError(RuntimeError):
    pass


class AsyncCall(object):
    def __init__(self, fnc, callback=None):
        self.Callable = fnc
        self.Callback = callback

    def __call__(self, *args, **kwargs):
        self.Thread = threading.Thread(target=self.run, name=self.Callable.__name__, args=args, kwargs=kwargs)
        self.Thread.start()
        return self

    def wait(self, timeout=None):
        self.Thread.join(timeout)
        if self.Thread.isAlive():
            raise TimeoutError()
        else:
            return self.Result

    def run(self, *args, **kwargs):
        self.Result = self.Callable(*args, **kwargs)
        if self.Callback:
            self.Callback(self.Result)


class AsyncMethod(object):
    def __init__(self, fnc, callback=None):
        self.Callable = fnc
        self.Callback = callback

    def __call__(self, *args, **kwargs):
        return AsyncCall(self.Callable, self.Callback)(*args, **kwargs)


def Async(fnc=None, callback=None):
    if fnc == None:
        def AddAsyncCallback(fnc):
            return AsyncMethod(fnc, callback)

        return AddAsyncCallback
    else:
        return AsyncMethod(fnc, callback)


def launch(slice):
    _session = Session()
    urls = []
    for s in _session.query(BaseSite).filter(BaseSite.is_active == True).all():#.filter(BaseSite.is_active == True).slice(2, 3):  # .slice(1,100):
        urls.append({'id': s.id, 'url': s.rss})
    _session.close()
    for s in urls:
        parser(s)

@Async
def create_link_from_word_column(base_link, user_link, column):
    print('create_link_from_word_column(base_link, user_link)', base_link, user_link, column)
    """
    st = Session()
    columns_all = st.execute(text("SELECT * FROM column_to_column WHERE column_to_id = "+str(column.id)+";"))#st.query(column_to_column).filter(column_base_id==column.id)
    print('columns_all', columns_all)
    for col in columns_all:
        s = Session()
        column = s.query(UserColumn).filter(UserColumn.id == col.column_base_id, UserColumn.type == 2,
                                          UserColumn.is_active == True).first()

        if column:
            settings = s.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()
            print('have column', settings)

            link = s.query(UserColumnLink).filter(UserColumnLink.word == True,
                                              UserColumnLink.link_id == base_link.id,
                                              UserColumnLink.column_id == column.id).all()
            if not link:
                print('not link', link)
                text1 = base_link.title + ' ' + base_link.description
                sravni = calculation_to_word(text1, settings.word_filter, settings.word_exclude)

                if sravni:
                    print('sravni true', sravni)
                    user_link = UserColumnLink(user_id=column.user_id, site_id=base_link.site_id, link_id=base_link.id,
                                       site=False, word=True, show=True, column_id=column.id, is_active=True,
                                               created=datetime.datetime.now())

                    s.add(user_link)
                    s.commit()

        s.close()

    st.close()
    """

@Async
def update_user_link(link, site):
    st = Session()
    now = datetime.datetime.utcnow()
    print('base_linkbase_link start', id)

    res = st.execute("SELECT user_id, wall_id FROM user_wall_settings WHERE sources @> '{\"sites\": [%s]}';" % str(site))
    for r in res.fetchall():
        st.execute("INSERT INTO user_wall_link (user_id, wall_id, link_id, site_id, is_active, created) "
                   "VALUES ({0},{1},{2},{3}, TRUE, '{4}') ;".format(r[0], r[1], link, site, now))
        st.commit()



def parser(url):
    feed = feedparser.parse(url['url'])
    for entry in feed.entries:#[:2]:
        try:
            session = Session()
            link = session.query(BaseLink).filter(BaseLink.url == entry.link).all()
            if not link:
                link, site = func_entry(entry, url['url'], url['id'], session)
                if link:
                    update_user_link(link, site)
        except Exception as e:
            print('Exception', e)

request_headers = {
    "Accept-Language": "en-US,en;q=0.5",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Referer": "http://thewebsite.com",
    "Connection": "keep-alive"
}


def handle_url(link):
    data = {}
    request = requests.get(link, headers=request_headers)
    soup = BeautifulSoup(request.text)

    title = None
    try:
        try:
            title = soup.find("title").text
        finally:
            title = soup.find("meta", {"property": "og:title"})['content']
    except:
        pass
    description = None
    try:
        try:
            description = soup.find("meta", {"name": "description"})['content']
        finally:
            description = soup.find("meta", {"property": "og:description"})['content']
    except:
        pass
    keywords = None
    try:
        try:
            keywords = soup.find("meta", {"name": "keywords"})['content']
        finally:
            keywords = soup.find("meta", {"property": "og:keywords"})['content']
    except:
        pass
    img = None
    try:
        try:
            img = soup.find("meta", {"property": "og:image"})['content']
        finally:
            keywords = soup.find("meta", {"property": "og:keywords"})['content']
    except:
        pass

    img = soup.find("meta", {"property": "og:image"})['content']
    name = slugify(title) + '_test.jpg'
    nm = 'C:\project\contter\/app\media\_' + name
    f = open(nm, 'wb')
    img_content = requests.get(img, headers=request_headers)
    f.write(img_content.content)
    f.close()

    nwm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_thumb_' + name
    with Image(filename=nm) as img:
        img.transform(resize='27x')
        img.format = 'jpeg'
        img.save(filename=nwm)

    thumbnail = '_thumb_' + name
    imager_300 = '_' + name
    imager_600 = '_' + name
    imager_900 = '_' + name

    data = {"title": title, "description": description, "keywords": keywords, "url": link,
            "medium_img": imager_600, "thumbnail": thumbnail, "soup": str(soup)}
    #site_id=id, title=title, description=description, keywords=keywords, is_active=True,
    #                    url=entry['link'], full_url=url, medium_img=imager_600, thumbnail=thumbnail

    return data


def func_entry(entry, url, id, session):
        request = requests.get(entry['link'], headers=request_headers)

        soup = BeautifulSoup(request.text)

        title = None
        try:
            try:
                title = soup.find("title").text
            finally:
                title = soup.find("meta", {"property": "og:title"})['content']
        except:
            pass

        description = None
        try:
            try:
                description = soup.find("meta", {"name": "description"})['content']
            finally:
                description = soup.find("meta", {"property": "og:description"})['content']
        except:
            pass

        keywords = None
        try:
            try:
                keywords = soup.find("meta", {"name": "keywords"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass



        img = None
        try:
            try:
                img = soup.find("meta", {"property": "og:image"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass
        img = soup.find("meta", {"property": "og:image"})['content']
        name = slugify(title) + '_test.jpg'
        nm = 'C:\project\contter\/app\media\_' + name
        f = open(nm, 'wb')
        img_content = requests.get(img, headers=request_headers)
        f.write(img_content.content)
        f.close()

        desc = ''
        if description:
            desc = description

        try:
            print(title)
            print(desc)
        except:
            pass
        json_img = {}

        nwm = 'C:\OpenServer2\OpenServer\domains\contter.dev\media\img\_thumb_' + name
        with Image(filename=nm) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = 'jpeg'
            img.save(filename=nwm)

        thumbnail = '_thumb_' + name

        imager_300 = '_' + name
        imager_600 = '_' + name
        imager_900 = '_' + name

        link = BaseLink(site_id=id, title=title, description=description, keywords=keywords, is_active=True,
                        url=entry['link'], full_url=url, medium_img=imager_600, thumbnail=thumbnail)

        link_info = BaseLinkInfo(link_id=link.id, html=str(soup))
        session.add(link, link_info)
        session.commit()

        return link.id, id



if __name__ == '__main__':
    print('LAUNCH')
    slice = 2, 3
    launch(slice)
    print('END LAUNCH')
