import datetime
from sqlalchemy import desc, asc, text
from sqlalchemy.orm import sessionmaker
from models import engine, User, BaseSite, UserWall, UserColumn, BaseLink, UserColumnLink, UserColumnSettings, \
    UserColumnWallSort
Session = sessionmaker(bind=engine)

session = Session()

def just():
    now = datetime.datetime.now()
    past = now - datetime.timedelta(days=1)
    print('now', now)
    print('past', past.strftime("%Y%.m%.d.%H:%M:%S"))
    psd = datetime.datetime(past.year, past.month, past.day)
    site_ids = [2, 3, 1, 7]
    sts = session.execute(text("SELECT * FROM base_link WHERE is_active = TRUE AND site_id IN (" + ','.join((str(n) for n in site_ids)) + ") AND created <= '{0}' ORDER BY counter DESC LIMIT 20; ".format(psd)))
    for st in sts:
        print('st', st.counter)

    session.commit()
    session.close()

if __name__ == '__main__':
    print('LAUNCH')
    just()
    print('END LAUNCH')