import urllib2

import feedparser

from slugify import slugify
from threading import Thread
import subprocess
from Queue import Queue

import time

from bs4 import BeautifulSoup
from sqlalchemy.orm import sessionmaker, scoped_session

from imager import imager
from models import engine, User, BaseLink, BaseSite

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
_session = Session()

num_threads = 100
queue = Queue()
ips = ["http://bleacherreport.com/articles/feed", "http://sports.espn.go.com/espn/rss/nhl/news", "http://www.sportsnet.ca/wp-content/themes/sportsnet/images/sn_default_logo.gif", "http://www.cbc.ca/cmlink/rss-sports-nhl"]

def func_entry(entry, url, id):
    try:
           print(entry.link)
           #print(entry.title)
           #print(entry.description)

           page = urllib2.urlopen(entry.link)
           soup = BeautifulSoup(page)

           title = None
           try:
              try:
                 title = soup.find("title").text
              finally:
                 title = soup.find("meta", {"property": "og:title"})['content']
           except:
              pass

           description = None
           try:
              try:
                  description = soup.find("meta", {"name": "description"})['content']
              finally:
                  description = soup.find("meta", {"property": "og:description"})['content']
           except:
              pass

           keywords = None
           try:
               try:
                   keywords = soup.find("meta", {"name": "keywords"})['content']
               finally:
                   keywords = soup.find("meta", {"property": "og:keywords"})['content']
           except:
               pass
           print(title, description, keywords)

           print('soup', soup.find("meta", {"property": "og:image"})['content'])
           img = soup.find("meta", {"property": "og:image"})['content']
           name = slugify(title)+'_test.jpg'
           nm = 'C:\Users\user\PycharmProjects\contest2\img\_'+ name
           f = open(nm, 'wb')
           f.write(urllib2.urlopen(img).read())
           f.close()
           imager(name, title)

           imager_300 = '_300_'+name
           imager_600 = '_600_'+name
           imager_900 = '_900_'+name

           #save base
           #

           ed_user = BaseLink(site_id=id, title=title, description=description, keywords=keywords, is_active=True,
                              url=entry.link, full_url=url, small_img=imager_300,
                              medium_img=imager_600, large_img=imager_900, tags='')

           _session.add(ed_user)




           print(' ')
    except:
            pass

def parser(url):
    feed = feedparser.parse(url['url'])
    for entry in feed.entries:
        try:

            link = _session.query(BaseLink).filter(BaseLink.url == entry.link).all()
            if not link:
                func_entry(entry, url['url'], url['id'])
                _session.commit()
                #pass
            print(link)
            print(entry.link)
        except:
            pass

def pinger(i, q):
    """Pings subnet"""
    print(i, q)
    while True:
        ip = q.get()
        print "Thread %s: Pinging %s" % (i, ip)
        parser(ip)
        q.task_done()

for i in range(num_threads):

    worker = Thread(target=pinger, args=(i, queue))
    worker.setDaemon(True)
    worker.start()

for ip in _session.query(BaseSite).filter(BaseSite.is_active==True).slice(1,100):
    queue.put({'id': ip.id, 'url': ip.rss})





if __name__ == '__main__':
    print('START COMMAND')
    start = time.time()
    print "Main Thread Waiting"
    queue.join()

    print(time.time() - start)
    Session.remove()
    print('END COMMAND')


