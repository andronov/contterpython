from sqlalchemy.orm import scoped_session, sessionmaker
import tornado.web
from models import *  # import the engine to bind

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
             (r"/", BaseHandler),
        ]
        settings = dict(
            cookie_secret="some_long_secret_and_other_settins"
        )
        tornado.web.Application.__init__(self, handlers, **settings)
        # Have one global connection.
        self.db = scoped_session(sessionmaker(bind=engine))

class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

    def get_current_user(self):
        user_id = self.get_secure_cookie("user")
        if not user_id: return None
        return self.db.query(User).get(user_id)