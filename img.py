from wand.image import Image
from wand.drawing import Drawing
from wand.color import Color


def img(name, text):

    nm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(name)

    i = 900
    while i <= 900:
        new_name = 'NEW_nhlF_'+str(i)+'_'+name
        nwm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(new_name)
        with Image(filename=nm) as img:
            print('width =', img.width)
            print('height =', img.height)
            #img.crop(width=300, height=250, gravity='center')
            #img.transform(resize='600x')
            img.transform(resize=str(i)+'x')
            #img.level(0.2, 0.9, gamma=1.1)
            #img.format = 'jpeg'
            #img.save(filename='test2.jpg')
            with Drawing() as draw:
                # set draw.fill_color here? YES

                draw.fill_color = Color('#000000')
                draw.fill_opacity = 0.50
                sum = img.height - 110
                print(sum)
                draw.rectangle(left=0, top=sum, width=img.width, height=270)
                draw(img)
                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 42
                if i == 300:
                    draw.font_size = 12
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(50, img.height - 60, text)
                draw(img)

                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 26
                if i == 300:
                    draw.font_size = 12
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(50, img.height - 30, 'Rangers forward lifts team on his third goal of the night.')
                draw(img)

                #image.format = 'gif'
                img.format = 'jpeg'
                img.save(filename=nwm)
        i += 300

def img2(name, text):

    nm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(name)

    i = 600
    while i <= 600:
        new_name = 'NEW_nhlF_'+str(i)+'_'+name
        nwm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(new_name)
        with Image(filename=nm) as img:
            print('width =', img.width)
            print('height =', img.height)
            #img.crop(width=300, height=250, gravity='center')
            #img.transform(resize='600x')
            img.transform(resize=str(i)+'x')
            #img.level(0.2, 0.9, gamma=1.1)
            #img.format = 'jpeg'
            #img.save(filename='test2.jpg')
            with Drawing() as draw:
                # set draw.fill_color here? YES

                draw.fill_color = Color('#000000')
                draw.fill_opacity = 0.50
                sum = img.height - 110
                print(sum)
                draw.rectangle(left=0, top=sum, width=img.width, height=270)
                draw(img)
                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 42
                if i == 300:
                    draw.font_size = 12
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(50, img.height - 60, text)
                draw(img)

                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 26
                if i == 300:
                    draw.font_size = 12
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(50, img.height - 30, 'Rangers forward lifts team on his third goal of the night.')
                draw(img)

                #image.format = 'gif'
                img.format = 'jpeg'
                img.save(filename=nwm)
        i += 300

def img3(name, text):

    nm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(name)

    i = 300
    while i <= 300:
        new_name = 'NEW_nhlF_'+str(i)+'_'+name
        nwm = 'C:\Users\user\PycharmProjects\contest2\itest\_'+str(new_name)
        with Image(filename=nm) as img:
            print('width =', img.width)
            print('height =', img.height)
            #img.crop(width=300, height=250, gravity='center')
            #img.transform(resize='600x')
            img.transform(resize=str(i)+'x')
            #img.level(0.2, 0.9, gamma=1.1)
            #img.format = 'jpeg'
            #img.save(filename='test2.jpg')
            with Drawing() as draw:
                # set draw.fill_color here? YES

                draw.fill_color = Color('#000000')
                draw.fill_opacity = 0.50
                sum = img.height - 42
                print(sum)
                draw.rectangle(left=0, top=sum, width=img.width, height=270)
                draw(img)
                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 42
                if i == 300:
                    draw.font_size = 13
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(5, img.height - 25, text)
                draw(img)

                #draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font = 'C:\Users\user\PycharmProjects\contest\HelveticaNeueCyr-Medium.otf'
                draw.font_size = 16
                if i == 900:
                    draw.font_size = 26
                if i == 300:
                    draw.font_size = 10
                draw.fill_color = Color('#ffffff')
                #draw.color(Color('#000000'))
                draw.text(5, img.height - 10, 'Rangers forward lifts team on his third goal of the night.')
                draw(img)

                #image.format = 'gif'
                img.format = 'jpeg'
                img.save(filename=nwm)
        i += 300

def launch():
    img('itest.jpg', 'Nash records Hat Trick on overtime winner')
    img2('itest.jpg', 'Nash records Hat Trick on overtime winner')
    img3('itest.jpg', 'Nash records Hat Trick on overtime winner')

if __name__ == '__main__':
    launch()