

import tornado.web
import tornado.ioloop
import tornado.options
import tornado.httpserver
from tornado import gen

from admin.flasky import anonymous_authenticateded


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db


class AdminTestHandler(BaseHandler):

    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminTestHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test admin')
        self.finish()