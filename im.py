# -*- coding: utf-8 -*-
import urllib2
from pprint import pprint

import re
from bs4 import BeautifulSoup
from wand.image import Image
from wand.drawing import Drawing
from wand.color import Color
import textwrap



def imager(name, text, desc):

    nm = 'C:\im\_'+str(name)

    i = 300
    while i <= 300:
        new_name = '_'+str(i)+'_'+name
        nwm = 'C:\im\_'+str(new_name)
        with Image(filename=nm) as img:
            print('width =', img.width)
            print('height =', img.height)
            #img.crop(width=300, height=250, gravity='center')
            #img.transform(resize='600x')
            img.transform(resize=str(i)+'x')
            #img.level(0.2, 0.9, gamma=1.1)
            #img.format = 'jpeg'
            #img.save(filename='test2.jpg')
            with Drawing() as draw:
                # set draw.fill_color here? YES

                draw.fill_color = Color('#000000')
                draw.fill_opacity = 0.12
                #sum = img.height - 70
                #print(sum)
                draw.rectangle(left=0, top=2, width=img.width, height=img.height-2)
                draw(img)

                """title"""

                #width, height = font.getsize(line)
                #draw.text(((w - width) / 2, y_text), line, font=font, fill=FOREGROUND)

                draw.font = 'C:\Users\user\PycharmProjects\contest\calibrib.otf'
                draw.font_size = 16
                font_top = 50
                font_left = 20
                if i == 900:
                    draw.font_size = 46
                    font_top = 37
                    font_left = 22
                    w_title = 30
                    steper = 41
                if i == 300:
                    draw.font_size = 16
                    font_top = 37
                    font_left = 22
                    w_title = 30
                    steper = 31
                if i == 600:
                    draw.font_size = 26
                    font_top = 37
                    font_left = 22
                    w_title = 39
                    steper = 41

                text = textwrap.fill(text, initial_indent='', subsequent_indent='', width=w_title)
                step = len(re.findall("\n", text, re.IGNORECASE))


                draw.fill_color = Color('#ffffff')
                draw.font_weight = 600
                draw.text(font_left, font_top, text)
                draw(img)
                """end title"""

                """desc"""

                #width, height = font.getsize(line)
                #draw.text(((w - width) / 2, y_text), line, font=font, fill=FOREGROUND)

                draw.font = 'C:\Users\user\PycharmProjects\contest\calibrii0.otf'
                draw.font_size = 16

                font_left = 20
                if i == 900:
                    draw.font_size = 32
                    font_top = 80 + (font_top + (steper*step))#22
                    font_left = 20
                    w_desc = 53
                if i == 300:
                    draw.font_size = 12
                    font_top = 5 + (font_top + (steper*step))
                    font_left = 20
                    w_desc = 42
                if i == 600:
                    draw.font_size = 18
                    font_top = 22 + (font_top + (steper*step))
                    font_left = 20
                    w_desc = 55


                text = textwrap.fill(desc, initial_indent='', subsequent_indent='', width=w_desc)
                t, i = u'', 1
                for f in text.split('\n'):
                    if i <= 3:
                        if i == 3:
                            t += f + '...'
                        else:
                            t += f + '\n'
                    i += 1


                draw.fill_color = Color('#ffffff')
                #draw.font_weight = 100
                draw.font_style = 'italic'
                draw.text(font_left, font_top, t)
                draw(img)
                """end desc"""

                #image.format = 'gif'
                img.format = 'jpeg'
                img.save(filename=nwm)
        i += 300

def launch():
    #page = urllib2.urlopen('')
    #soup = BeautifulSoup(page)
    img = 'http://img.championat.com/news/big/o/j/so-vtoroj-popytki-real-i-roma-poradujut-rezultativnoj-igroj_14574317331149649623.jpg'
    name = 'testtesttest' + '_test.jpg'
    nm = 'C:\im\_' + name
    f = open(nm, 'wb')
    print('ffffff', f)
    f.write(urllib2.urlopen(img).read())
    f.close()

    text = u'Конте попросил «Челси» приобрести Пьянича '
    desc = u'Полузащитник «Ромы» Миралем Пьянич вошёл в трансферный список «Челси», подготовленный будущим главным тренером синих Антонио Конте, утверждает La Gazzetta dello Sport.'


    imager(name, text, desc)


if __name__ == '__main__':
    launch()
    #lines = textwrap.wrap("LAPD has knife allegedly from Simpson property LAPD has knife allegedly from Simpson property", width=270)
    #for line in lines:
    #    print line
    body = "LAPD has knife allegedly from Simpson property LAPD has knife allegedly from Simpson property"
    new_body = ""
    lines = body.split("\n")

    for line in lines:
        if len(line) > 270:
            w = textwrap.TextWrapper(width=270, break_long_words=False)
            line = '\n'.join(w.wrap(line))

        new_body += line + "\n"

    pprint(new_body)
    pprint(textwrap.fill(body, initial_indent='', subsequent_indent='', width=30))
    pass
