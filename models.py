import enum
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
from sqlalchemy.orm import relationship, backref
from sqlalchemy_utils import ChoiceType
from werkzeug.security import generate_password_hash, check_password_hash

engine = sa.create_engine('postgresql+psycopg2://postgres:rjierb18@localhost:5433/boston', echo=False)

Base = declarative_base()

"""
#
# Abstract Base model
#
"""


class CommonRoutines(Base):
    __abstract__ = True

    is_active = sa.Column(sa.Boolean, unique=False, default=False)
    updated = sa.Column(sa.DateTime)
    created = sa.Column(sa.DateTime, default=sa.func.now())


class User(CommonRoutines):
    TYPE_TYPES = [
        (1, 'public'),
        (2, 'private')
    ]
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.String)
    first_name = sa.Column(sa.String, nullable=True)
    last_name = sa.Column(sa.String, nullable=True)
    password = sa.Column(sa.String)
    email = sa.Column(sa.String, nullable=True)

    type = sa.Column(sa.Integer, nullable=True, default=1)

    display_name = sa.Column(sa.String, nullable=True)
    twitter = sa.Column(sa.String, nullable=True)
    twitter_img = sa.Column(sa.String, nullable=True)

    color = sa.Column(sa.String, nullable=True)

    description = sa.Column(sa.String, nullable=True)
    avatar = sa.Column(sa.String, nullable=True)

    new = sa.Column(sa.Boolean, unique=False, default=True)

    last_visit = sa.Column(sa.DateTime)
    city = sa.Column(sa.String(), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    official = sa.Column(sa.Boolean, unique=False, default=False)
    official_info = sa.Column(sa.String, nullable=True)

    location = sa.Column(sa.String, nullable=True)
    website = sa.Column(sa.String, nullable=True)
    timezone = sa.Column(sa.String, nullable=True)
    country = sa.Column(sa.String, nullable=True)

    followers = sa.Column(sa.Integer(), default=0, nullable=True)

    is_ban = sa.Column(sa.Boolean, unique=False, default=False)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User: {}>'.format(self.id)


class UserSettings(CommonRoutines):
    __tablename__ = 'user_settings'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    extra = sa.Column(JSONB, nullable=True)


class UserRelationship(CommonRoutines):
    __tablename__ = 'user_relationship'

    id = sa.Column(sa.Integer, primary_key=True)

    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    user_from_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)


class UserWallRelationship(CommonRoutines):
    __tablename__ = 'user_wall_relationship'

    id = sa.Column(sa.Integer, primary_key=True)

    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    user_from_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=False)


class UserBlackList(CommonRoutines):
    __tablename__ = 'user_black_list'

    id = sa.Column(sa.Integer, primary_key=True)

    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    user_from_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)


class UserReport(CommonRoutines):
    __tablename__ = 'user_report'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)

    msg = sa.Column(sa.String, nullable=True)


class UserNotifications(CommonRoutines):
    __tablename__ = 'user_notifications'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True)


class BaseSite(CommonRoutines):
    __tablename__ = 'base_site'

    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    url = sa.Column(sa.String, nullable=True)
    short_url = sa.Column(sa.String, nullable=True)
    rss = sa.Column(sa.String, nullable=True)  # url rss
    image = sa.Column(sa.String, nullable=True)
    favicon = sa.Column(sa.String, nullable=True)
    color = sa.Column(sa.String, nullable=True)
    followers = sa.Column(sa.Integer(), default=0, nullable=True)
    lang = sa.Column(sa.String, nullable=True, default='en')

    parent = sa.Column(sa.Boolean, unique=False, default=True)
    parent_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)

    custom = sa.Column(sa.Boolean, default=True)


class UserBaseSite(CommonRoutines):
    __tablename__ = 'user_base_site'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=False)

    role = sa.Column(sa.String, nullable=True)

    is_super = sa.Column(sa.Boolean, unique=False, default=False)


class BaseSiteInfo(CommonRoutines):
    __tablename__ = 'base_site_info'

    id = sa.Column(sa.Integer, primary_key=True)

    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=False)
    extra = sa.Column(JSONB, nullable=True)


class BaseLinkDynamic(CommonRoutines):
    __tablename__ = 'base_link_dynamic'

    id = sa.Column(sa.Integer, primary_key=True)

    content = sa.Column(sa.String, nullable=True)
    extra = sa.Column(JSONB, nullable=True)


class BaseLink(CommonRoutines):
    TYPE_TYPES = [
        (1, 'standard'),
        (2, 'dynamic'),
        (3, 'post'),
        (4, 'custom')
    ]
    __tablename__ = 'base_link'

    id = sa.Column(sa.Integer, primary_key=True)

    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)

    title = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    keywords = sa.Column(sa.String, nullable=True)

    type = sa.Column(sa.Integer, nullable=True, default=1)
    dynamic_id = sa.Column(sa.Integer, sa.ForeignKey('base_link_dynamic.id'), nullable=True)

    url = sa.Column(sa.String, nullable=True)
    full_url = sa.Column(sa.String, nullable=True)

    thumbnail = sa.Column(sa.String, nullable=True)
    extra_img = sa.Column(sa.String, nullable=True)
    small_img = sa.Column(sa.String, nullable=True)
    medium_img = sa.Column(sa.String, nullable=True)
    large_img = sa.Column(sa.String, nullable=True)
    site_img = sa.Column(sa.String, nullable=True)
    site_img_url = sa.Column(sa.String, nullable=True)
    json_img = sa.Column(JSONB, nullable=True)

    counter = sa.Column(sa.Integer, default=0, nullable=True)

    tags = sa.Column(sa.String, nullable=True)

    show = sa.Column(sa.Boolean, unique=False, default=True)


class BaseLinkInfo(CommonRoutines):
    __tablename__ = 'base_link_info'

    id = sa.Column(sa.Integer, primary_key=True)

    link_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    extra = sa.Column(JSONB, nullable=True)
    html = sa.Column(sa.String, nullable=True)


class UserWall(CommonRoutines):
    TYPE_TYPES = [
        (1, 'public'),
        (2, 'private'),
        (3, 'follow'),
        (4, 'liked'),
        (5, 'profile'),
    ]
    __tablename__ = 'user_wall'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    color = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)

    follow_user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    follow_wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    follow = sa.Column(sa.Boolean, unique=False, default=False)

    type = sa.Column(sa.Integer, nullable=True, default=1)

    icon = sa.Column(sa.String, nullable=True)

    sort = sa.Column(sa.Integer, nullable=True)
    category_id = sa.Column(sa.Integer, nullable=True)

    thumbnail = sa.Column(sa.String, nullable=True)
    medium_img = sa.Column(sa.String, nullable=True)

    home = sa.Column(sa.Boolean, unique=False, default=False)


class UserWallSettings(CommonRoutines):
    __tablename__ = 'user_wall_settings'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)

    word_filter = sa.Column(sa.String, nullable=True)
    word_exclude = sa.Column(sa.String, nullable=True)

    words = sa.Column(JSONB, nullable=True)
    sources = sa.Column(JSONB, nullable=True)

    realtime = sa.Column(sa.Boolean, unique=False, default=False)


class UserWallLink(CommonRoutines):
    __tablename__ = 'user_wall_link'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    link_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)

    shared_id = sa.Column(sa.Integer, nullable=True)
    shared = sa.Column(sa.Boolean, unique=False, default=False)
    shared_add = sa.Column(sa.DateTime, nullable=True)

    showed = sa.Column(sa.Boolean, unique=False, default=False)
    showed_add = sa.Column(sa.DateTime, nullable=True)

    is_ban = sa.Column(sa.Boolean, unique=False, default=False)

    counter = sa.Column(sa.Integer, nullable=True)

    liked = sa.Column(sa.Boolean, unique=False, default=False)
    liked_add = sa.Column(sa.DateTime, nullable=True)
    saved = sa.Column(sa.Boolean, unique=False, default=False)
    saved_add = sa.Column(sa.DateTime, nullable=True)


class UserWallLinkReport(CommonRoutines):
    __tablename__ = 'user_wall_link_report'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    link_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    user_link_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall_link.id'), nullable=True)

    msg = sa.Column(sa.String, nullable=True)


class SiteFromUser(CommonRoutines):
    __tablename__ = 'site_from_user'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    processed = sa.Column(sa.Boolean, unique=False, default=False)
    value = sa.Column(sa.String, nullable=True)


class UserWallNotification(CommonRoutines):
    __tablename__ = 'user_wall_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True)  # day, week, month


class UserNotification(CommonRoutines):
    TYPE_TYPES = [
        (1, 'following'),
        (2, 'liked'),
        (3, 'shared'),
        (4, 'mentioned'),
        (99, 'other'),
    ]
    __tablename__ = 'user_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)

    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_to_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    link_to_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    wall_to_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    user_link_to_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall_link.id'), nullable=True)

    type = sa.Column(ChoiceType(TYPE_TYPES, impl=sa.Integer()), nullable=True)
    action = sa.Column(sa.String, nullable=True)

    read = sa.Column(sa.Boolean, unique=False, default=False)

    msg = sa.Column(sa.String, nullable=True)

"""
class Hashtag(CommonRoutines):
    TYPE_TYPES = [
        (1, 'base'),
    ]
    __tablename__ = 'hashtag'
    id = sa.Column(sa.Integer, primary_key=True)

    type = sa.Column(ChoiceType(TYPE_TYPES, impl=sa.Integer()), nullable=True, default=1)

    name = sa.Column(sa.String, nullable=True)
    short_name = sa.Column(sa.String, nullable=True)

    is_bad = sa.Column(sa.Boolean, unique=False, default=False)
    is_show = sa.Column(sa.Boolean, unique=False, default=False)
    is_adult = sa.Column(sa.Boolean, unique=False, default=False)
    is_private = sa.Column(sa.Boolean, unique=False, default=False)

    counter = sa.Column(sa.Integer, nullable=True, default=1)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)


class HashtagToLink(CommonRoutines):
    TYPE_TYPES = [
        (1, 'base'),
    ]
    __tablename__ = 'hashtag_to_link'
    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=True)

    hashtag_id = sa.Column(sa.Integer, sa.ForeignKey('hashtag.id'), nullable=True)
    link_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    column_link_id = sa.Column(sa.Integer, sa.ForeignKey('user_column_link.id'), nullable=True)

    is_show = sa.Column(sa.Boolean, unique=False, default=False)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)

    counter = sa.Column(sa.Integer, nullable=True, default=1)

"""

metadata = Base.metadata


def create_all():
    print('create')
    metadata.create_all(engine)


if __name__ == '__main__':
    create_all()