#!/usr/bin/env python
"""A simplistic Twitter viewer to demonstrate the use of TwitterMixin.

To run this app, you must first register an application with Twitter:
  1) Go to https://dev.twitter.com/apps and create an application.
     Your application must have a callback URL registered with Twitter.
     It doesn't matter what it is, but it has to be there (Twitter won't
     let you use localhost in a registered callback URL, but that won't stop
     you from running this demo on localhost).
  2) Create a file called "secrets.cfg" and put your consumer key and
     secret (which Twitter gives you when you register an app) in it:
       twitter_consumer_key = 'asdf1234'
       twitter_consumer_secret = 'qwer5678'
     (you could also generate a random value for "cookie_secret" and put it
     in the same file, although it's not necessary to run this demo)
  3) Run this program and go to http://localhost:8888 (by default) in your
     browser.
"""

import logging

from tornado.auth import TwitterMixin
from tornado.escape import json_decode, json_encode
from tornado.ioloop import IOLoop
from tornado import gen
from tornado.options import define, options, parse_command_line, parse_config_file
from tornado.web import Application, RequestHandler, authenticated

define('port', default=8888, help="port to listen on")
define('config_file', default='secrets.cfg',
       help='filename for additional configuration')

define('debug', default=False, group='application',
       help="run in debug mode (with automatic reloading)")
# The following settings should probably be defined in secrets.cfg
define('twitter_consumer_key', default='5iPsfve5Nmd6Kn0wxEIFGhvjj', type=str, group='application')
define('twitter_consumer_secret', default='WCfOQvgCOM0Xq6HfvsjL7n7oOIlZkac4EH9c1HgkL96vJ0kc8H', type=str, group='application')
define('cookie_secret', default='PnezBjwAAAAAAdg2sAAABURcfF3Q', type=str, group='application',
       help="signing key for secure cookies")

class BaseHandler(RequestHandler):
    COOKIE_NAME = 'twitterdemo_user'

    def get_current_user(self):
        user_json = {'username': u'andr_andron', u'follow_request_sent': False, u'has_extended_profile': False, u'profile_use_background_image': True, u'default_profile_image': False, u'id': 2897048110L, u'profile_background_image_url_https': u'https://abs.twimg.com/images/themes/theme1/bg.png', u'verified': False, u'profile_text_color': u'333333', u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/554066257083760640/2BheMQBE_normal.jpeg', u'profile_sidebar_fill_color': u'DDEEF6', u'entities': {u'description': {u'urls': []}}, u'followers_count': 12, u'profile_sidebar_border_color': u'C0DEED', u'id_str': u'2897048110', u'profile_background_color': u'C0DEED', u'listed_count': 0, u'status': {u'contributors': None, u'truncated': False, u'text': u'RT @VStognienko: \u041d\u0430\u0448 \u043d\u0435\u0441\u0447\u0430\u0441\u0442\u043d\u044b\u0439 \u0438 \u0443\u0436\u0430\u0441\u043d\u044b\u0439 \u043c\u0438\u0440 \u043e\u0441\u0442\u0430\u0435\u0442\u0441\u044f \u043f\u0440\u0435\u0436\u043d\u0438\u043c. \u041d\u0435\u0441\u043c\u043e\u0442\u0440\u044f \u043d\u0430 \u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442, \u0441\u043c\u0430\u0440\u0442\u0444\u043e\u043d\u044b \u0438 \u043f\u043e\u0441\u0442\u0438\u043d\u0434\u0443\u0441\u0442\u0440\u0438\u0430\u043b\u044c\u043d\u043e\u0435 \u043e\u0431\u0449\u0435\u0441\u0442\u0432\u043e', u'in_reply_to_status_id': None, u'id': 665433863653576704L, u'favorite_count': 0, u'source': u'<a href="http://twitter.com" rel="nofollow">Twitter Web Client</a>', u'retweeted': True, u'coordinates': None, u'entities': {u'symbols': [], u'user_mentions': [{u'id': 601237742, u'indices': [3, 15], u'id_str': u'601237742', u'screen_name': u'VStognienko', u'name': u'\u0412\u043b\u0430\u0434\u0438\u043c\u0438\u0440 \u0421\u0442\u043e\u0433\u043d\u0438\u0435\u043d\u043a\u043e'}], u'hashtags': [], u'urls': []}, u'in_reply_to_screen_name': None, u'id_str': u'665433863653576704', u'retweet_count': 133, u'in_reply_to_user_id': None, u'favorited': False, u'retweeted_status': {u'lang': u'ru', u'favorited': False, u'entities': {u'symbols': [], u'user_mentions': [], u'hashtags': [], u'urls': []}, u'contributors': None, u'truncated': False, u'text': u'\u041d\u0430\u0448 \u043d\u0435\u0441\u0447\u0430\u0441\u0442\u043d\u044b\u0439 \u0438 \u0443\u0436\u0430\u0441\u043d\u044b\u0439 \u043c\u0438\u0440 \u043e\u0441\u0442\u0430\u0435\u0442\u0441\u044f \u043f\u0440\u0435\u0436\u043d\u0438\u043c. \u041d\u0435\u0441\u043c\u043e\u0442\u0440\u044f \u043d\u0430 \u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442, \u0441\u043c\u0430\u0440\u0442\u0444\u043e\u043d\u044b \u0438 \u043f\u043e\u0441\u0442\u0438\u043d\u0434\u0443\u0441\u0442\u0440\u0438\u0430\u043b\u044c\u043d\u043e\u0435 \u043e\u0431\u0449\u0435\u0441\u0442\u0432\u043e', u'created_at': u'Sat Nov 14 07:13:17 +0000 2015', u'retweeted': True, u'in_reply_to_status_id_str': None, u'coordinates': None, u'in_reply_to_user_id_str': None, u'source': u'<a href="http://twitter.com/#!/download/ipad" rel="nofollow">Twitter for iPad</a>', u'in_reply_to_status_id': None, u'in_reply_to_screen_name': None, u'id_str': u'665427263991783424', u'place': None, u'retweet_count': 133, u'geo': None, u'id': 665427263991783424L, u'favorite_count': 121, u'in_reply_to_user_id': None}, u'geo': None, u'in_reply_to_user_id_str': None, u'lang': u'ru', u'created_at': u'Sat Nov 14 07:39:30 +0000 2015', u'in_reply_to_status_id_str': None, u'place': None}, u'is_translation_enabled': False, u'utc_offset': 14400, u'statuses_count': 71, u'description': u'', u'friends_count': 87, u'location': u'', u'profile_link_color': u'0084B4', u'profile_image_url': u'http://pbs.twimg.com/profile_images/554066257083760640/2BheMQBE_normal.jpeg', u'following': False, u'geo_enabled': False, u'profile_background_image_url': u'http://abs.twimg.com/images/themes/theme1/bg.png', u'screen_name': u'andr_andron', u'lang': u'ru', u'profile_background_tile': False, u'favourites_count': 31, u'name': u'Andrew Gerrard', u'notifications': False, u'url': None, u'created_at': u'Sat Nov 29 06:53:40 +0000 2014', u'contributors_enabled': False, u'time_zone': u'Baku', 'access_token': {'secret': 'C9iVuJkzO3YW9jt39DheZj0eur4WcuNjyuCY2jM8L1Vv6', 'user_id': '2897048110', 'x_auth_expires': '0', 'key': '2897048110-ANgnd1AAEtq9aaVCkkENrU6RRLvF5GxfyIuwnWO', 'screen_name': 'andr_andron'}, u'protected': False, u'default_profile': True, u'is_translator': False}

        print('uuuuu', user_json)
        if not user_json:
            return None
        return user_json

class MainHandler(BaseHandler, TwitterMixin):
    #@authenticated
    @gen.coroutine
    def get(self):
        print('self.current_user[access_token]', self.current_user['access_token'])
        timeline = yield self.twitter_request(
            '/statuses/home_timeline',
            access_token=self.current_user['access_token'])
        self.render('home.html', timeline=timeline)

class LoginHandler(BaseHandler, TwitterMixin):
    @gen.coroutine
    def get(self):
        if self.get_argument('oauth_token', None):
            user = yield self.get_authenticated_user()
            print(user)
            del user["description"]
            print(self.set_secure_cookie(self.COOKIE_NAME, json_encode(user)))
            self.set_secure_cookie(self.COOKIE_NAME, json_encode(user))
            self.redirect(self.get_argument('next', '/'))
        else:
            yield self.authorize_redirect(callback_uri=self.request.full_url())

class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie(self.COOKIE_NAME)

def main():
    parse_command_line(final=False)
    parse_config_file(options.config_file)

    app = Application(
        [
            ('/', MainHandler),
            ('/login', LoginHandler),
            ('/logout', LogoutHandler),
        ],
        login_url='/login',
        **options.group_dict('application'))
    app.listen(options.port)

    logging.info('Listening on http://localhost:%d' % options.port)
    IOLoop.current().start()

if __name__ == '__main__':
    main()
