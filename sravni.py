import difflib

import time
from sqlalchemy import desc, asc
from sqlalchemy.orm import sessionmaker
from models import engine, User, BaseSite, UserWall, UserColumn, BaseLink, UserColumnLink, UserColumnSettings

Session = sessionmaker(bind=engine)

session = Session()


def sravni():
    link = session.query(BaseLink).filter(BaseLink.id == 198).first()

    title = link.title
    description = link.description

    text1 = title + ' ' + description
    txt1 = text1.replace(",", "").replace(".", "").lower().split(' ')

    st = session.query(UserColumnSettings).filter(UserColumnSettings.id == 18).first()
    word_filter = st.word_filter
    word_exclude = st.word_exclude

    if not word_filter:
        wf = []
    else:
        wf = word_filter.lower().split(', ')
    if not word_exclude:
        we = []
    else:
        we = word_exclude.lower().split(', ')

    next = False

    if len(wf) > 0:
        sm_f = difflib.SequenceMatcher(None, txt1, wf)
        if sm_f.ratio() > 0:
            next = True
    else:
        if len(we) > 0:
            sm_e = difflib.SequenceMatcher(None, txt1, we)
            if sm_e.ratio() <= 0:
                next = True

    if len(wf) <= 0 and len(we) <= 0:
        next = True

    print(next)
    session.close()
    print('calculation_to_word(text1, word_filter, word_exclude)', calculation_to_word(text1, word_filter, word_exclude))


def calculation_to_word(text1, word_filter, word_exclude):
    next = False
    txt1 = text1.replace(",", "").replace(".", "").lower().split(' ')
    if not word_filter:
        wf = []
    else:
        wf = word_filter.lower().split(', ')
    if not word_exclude:
        we = []
    else:
        we = word_exclude.lower().split(', ')


    if len(wf) > 0:
        sm_f = difflib.SequenceMatcher(None, txt1, wf)
        if sm_f.ratio() > 0:
            next = True
    else:
        if len(we) > 0:
            sm_e = difflib.SequenceMatcher(None, txt1, we)
            if sm_e.ratio() <= 0:
                next = True

    if len(wf) <= 0 and len(we) <= 0:
        next = True

    return next

if __name__ == '__main__':
    start = time.time()
    sravni()
    print(time.time() - start)